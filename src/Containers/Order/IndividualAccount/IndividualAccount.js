import React, {useState} from 'react';
import InputGroup from "./InputGroup/InputGroup"


const IndividualAccount = () => {

    const [indivAccount, setIndivAccount] = useState({
        tip: 0,
        deliver: 0
    })
    //
    const [person, setPerson] = useState([
        {name: "Masha",
        sum: "200"},
        {name: "Dasha",
         sum: "300"}
    ]);

    const printValues = event => {
        const name = event.target.name;
        const value = event.target.value;
        const copyPerson = {...person};
        copyPerson[name] = value;
        setIndivAccount(copyPerson)
    }

    const printValues2 = event => {
        const name = event.target.name;
        const value = event.target.value;
        const copyIndivAccount = {...indivAccount};
        copyIndivAccount[name] = value;
        setIndivAccount(copyIndivAccount)
    }

    // const removeInput = id => {
    //     const index = --.findIndex(t => t.id === id);
    //     const - = [...-];
    //     -.splice(index, 1);
    //     -(-)
    // }

    const printSum = (event) => {
        event.preventDefault()
    }



    let arr = []
    const addForm = () => {
        arr.push(<InputGroup printValues={printValues}/>)
    };


    return (
        <form onSubmit={printSum}>
            <InputGroup printValues={printValues}/>
            <button onClick={addForm}>Add</button>
            <label>Процент чаевых:
                <input
                    type='number'
                    name="tip"
                    onChange={printValues2}
                /> %
            </label>
            <label>Доставка:
                <input
                    type='number'
                     name="deliver"
                    onChange={printValues2}
                />
            </label>
            <button>Рассчитать</button>
        </form>


    );
};

export default IndividualAccount;