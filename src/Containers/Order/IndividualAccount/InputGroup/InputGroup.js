import React from 'react';

const InputGroup = props => {
        return (
            <div>
                <label>
                    <input
                        type='text'
                        name="person"
                        onChange={props.printValues}
                    />
                </label>
                <br/>
                <label>
                    <input
                        type='number'
                        name="sum"
                        onChange={props.printValues}
                    /> сомов
                </label>
                <button>Delete</button>
            </div>
        )
};

export default InputGroup;