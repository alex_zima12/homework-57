import React, {useState} from 'react';
import TotalAccount from "./TotalAccount/TotalAccount";
import "./EquallyAccount.css"

const  EquallyAccount = () => {

    const [account,SetAccount] = useState({
        people: 0,
        sum: 0,
        tip: 0,
        deliver: 0
    })

    const [showTotal, setShowTotal] = useState(false);

    // const clickShowTotal = () => {
    //
    // }

    const printValues = event => {
        const name = event.target.name;
        const value = event.target.value;
        const copyAccount = {...account};
        copyAccount[name] = parseInt(value);
        SetAccount(copyAccount)

    }
    const printSum = (event ) => {
        event.preventDefault()
        setShowTotal(!showTotal)
    }

    let sum = null;

    if(showTotal){
        sum = (
            <TotalAccount account = {account}/>
        )
    }

    return (
        <>
        <form onSubmit={printSum} className="form">
            <label className="label">Человек:
                <input className="field"
                    type='number'
                    name="people"
                    onChange={printValues}
                />
            </label>
            <label className="label">Сумма заказа (сом):
                <input className="field sum"
                    type='number'
                    name="sum"
                   onChange={printValues}
                />
            </label>
            <label className="label">Процент чаевых:
                <input className="field"
                    type='number'
                    name="tip"
                    onChange={printValues}
                />
            </label>
            <label className="label">Доставка:
                <input className="field"
                    type='number'
                    // value={delivery}
                    name="deliver"
                    // onChange ={event => SetDelivery(event.target.value)}
                    onChange={printValues}
                />
            </label>
            <div> <button>Рассчитать</button></div>

        </form>
            {sum}
            </>

    );
};

export default EquallyAccount;