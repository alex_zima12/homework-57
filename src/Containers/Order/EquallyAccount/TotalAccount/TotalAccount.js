import React from 'react';
import "./totalAccount.css"

const TotalAccount = props => {
    let totalSum = (props.account.sum + (props.account.sum * props.account.tip/100) + props.account.deliver )/props.account.people
    if (props.account.sum > 0 && props.account.people > 0) {
        return (
      <div className="totalAccount">
          <p>Общая сумма:{props.account.sum} </p>
          <p>Количество человек:{props.account.people} </p>
          <p>Каждый платит по: {`${totalSum}`}  </p>
      </div>
    )}
    else {
        return <div className="totalAccount">Недопустимое значение</div>
    }
           };

export default TotalAccount;