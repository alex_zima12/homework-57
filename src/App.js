import React, {useState} from 'react';
import EquallyAccount from "./Containers/Order/EquallyAccount/EquallyAccount"
import './App.css';

import IndividualAccount from "./Containers/Order/IndividualAccount/IndividualAccount";

function App() {
    const [value, setValue] = useState ( '')

    const handleChange = event => {
        if (event.target.checked)
            setValue(event.target.value)
        setShowTotal(!showTotal)
    }

    const [showTotal, setShowTotal] = useState(false);

    let sum = null;


    if(showTotal){
        sum = (
            <EquallyAccount/>
        )
    }
    else {
        sum = (
        <IndividualAccount/>)
    }

    return (
        <div >
            <form className="container">
                    <strong>Выберите вариант:</strong>
                    <label>
                        <input
                            type = "radio"
                            checked={value === "поровну"}
                            value = "поровну"
                            onChange={handleChange}
                        />Поровну между всеми участниками
                    </label>
                    <label>
                        <input
                            type="radio"
                            checked={value === "индивидуально"}
                            value = "индивидуально"
                            onChange={handleChange}
                        />Каждому индивидуально
                    </label>
                </form>
            {sum}

        </div>
    )
};

export default App;
